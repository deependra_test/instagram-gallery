
(function($){

// basically just iterates over a collection, creates a new view per model and puts it in ul.
App.Views.UserDisplay = Backbone.View.extend({
	initialize: function(){	
			_.bindAll(this, 'render');
			this.render();		
	},
	
	render: function() {
		var $ul = $("<ul>").addClass("gallery");
		var col =this.collection;
		var len = col.length;
        var startPos = (this.options.page - 1) * 6;
        var endPos = Math.min(startPos + 6, len);
		var completeCollection = [];
		this.collection.each(function(user, index) {
			var item = new App.Views.User({model:user});
			completeCollection.push(item);	
		  })
	
		$("#lazyload").hide();
		for(var i= startPos ; i<endPos; i++)
		{
			var x= completeCollection[i];
			var renderedContent = x.render().el;		
			$ul.append(renderedContent);
		}
		$ul.append(new Paginator({model: completeCollection, page: this.options.page}).render().el);

		$container = $("#container");
		$container.empty();
		$container.append($ul);
    }
})

App.Views.User = Backbone.View.extend({
 	tagName: 'li',
	className: 'user_img',
 
	initialize: function(){	
			_.bindAll(this, 'render');
			this.template = _.template($('#caption-user-tmpl').html());
			this.render();		
	},
  
    render: function() {
		this.renderedContent = this.template(this.model.toJSON());
		$(this.el).html(this.renderedContent);
			
		return this; 	
    }
});

})(jQuery);
