window.Paginator = Backbone.View.extend({

    className: "pagination",

    initialize:function () {
        this.render();
    },

    render:function () {
        var items = this.model;
        var len = items.length;
        var pageCount = Math.ceil(len / 6);

        $(this.el).html('<ul />');
		var urltype= $('#urltype').val();
		if(urltype == 'feed')
		{
				for (var i=0; i < pageCount; i++) {
					$('ul', this.el).append("<li" + ((i + 1) === this.options.page ? " class='active'" : "") + "><a style='font-size:30px' href='#/feed/page/"+(i+1)+"'>" + (i+1) + "</a></li>");
				}
		}
		if(urltype == 'popular')
		{
				for (var i=0; i < pageCount; i++) {
					$('ul', this.el).append("<li" + ((i + 1) === this.options.page ? " class='active'" : "") + "><a style='font-size:30px' href='#/popular/page/"+(i+1)+"'>" + (i+1) + "</a></li>");
				}
		}

        return this;
    }
});
