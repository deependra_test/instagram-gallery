
// JavaScript Document/* routers */
App.Routers.Routes = Backbone.Router.extend({
		routes: {
			'': 'home',
			':id' : 'auth',
			'/popular/page/:page' : 'popular',
			'/feed/page/:page' : 'feed',
			'/tagsearch/:id': 'tagsearch',
		},
		
		home: function() {
			if (App.insta.render()) { // returns true if user is logged in.
				App.router.navigate("/feed/page/1", {trigger: true});
			} else {
				App.router.navigate("/popular/page/1", {trigger: true});	
			}
			
		},
		
		auth: function(id) {
			console.log("auth route" + id);
			var access_token = App.Helpers.getaccessToken(id);
			App.Helpers.createCookie('access_token',access_token,14);
			App.insta.cookiecheck();
			if (App.insta.render()){
				App.router.navigate("/feed/page/1", {trigger: true});	
			}
		},
	
	
		
		popular: function(page){
			$("#feedLink").removeClass("active");
			 $('#urltype').val('popular');
			 var p = page ? parseInt(page, 10) : 1;
				var endpoint ="media/popular";
				var len =20;
				var startPos = (p - 1) * 6;
		        var endPos = Math.min(startPos + 6, len);
				var $ul = $("#lazyloadul");
				for(var i= startPos ; i<endPos; i++)
				{
					$ul.append('<li class="user_img"><div class="wrapper"><a><img src="imgBack.jpg" style="width:306px; height:306px"></a></div></li>');
				}
				$("#lazyload").show();
				App.collections.popular = new App.Collections.Users([],{query: endpoint + "?"}); 
				App.collections.popular.fetch({success: function(users, response){
					App.views.popular = new App.Views.UserDisplay({collection: App.collections.popular,page:p}); 
					}
				 }); 	
				$("#popularLink").addClass("active");
		},
		
		feed: function(page) {
			$("#popularLink").removeClass("active");
			$('#urltype').val('feed');
            var p = page ? parseInt(page, 10) : 1;
			if (App.insta.render()) {  // returns true if user is logged in.
				var endpoint='users/self/feed/';
				var len =20;
				var startPos = (p - 1) * 6;
		        var endPos = Math.min(startPos + 6, len);
				var $ul = $("#lazyloadul");
				for(var i= startPos ; i<endPos; i++)
				{
					$ul.append('<li class="user_img"><div class="wrapper"><a><img src="imgBack.jpg" style="width:306px; height:306px"></a></div></li>');
				}
				$("#lazyload").show();
				App.collections.feed = [];
				App.collections.feed = new App.Collections.Users([],{query: endpoint + "?"});
				App.collections.feed.fetch({success: function(users, response){
					//alert(JSON.stringify(users));
					App.views.feed = new App.Views.UserDisplay({collection: App.collections.feed, page:p});
					}
				});
				$("#feedLink").addClass("active");
			}
		},
		
		tagsearch: function(id) {
			$('#urltype').val('');
			if (App.insta.render()) {  // returns true if user is logged in.
				var endpoint='tags';
				var query='id';
				App.collections.tags = new App.Collections.Users([],{query: endpoint + "/" + query + "/media/recent?"}); 
						App.collections.tags.fetch({success: function(users, response){
							App.views.tags = new App.Views.UserDisplay({collection: App.collections.tags,page:1});
						}
					 	});
			}
		}	
	})

App.router = new App.Routers.Routes();
