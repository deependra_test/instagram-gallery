App.Views.InstaView = Backbone.View.extend({ 
	initialize: function() {
		console.log("initialize app");
			this.cookiecheck();
	},
	
	
	render: function(){ // logged in
		if (App.settings.accesstoken){
			
			if (!this.header_auth) {
				this.header_auth = new App.Views.MenuAuth(); // show full menu
			}
			return true;
			
		} else { // not logged in
		
			console.log("not logged in");
			if (!this.header_noauth) {
				this.header_noauth = new App.Views.MenuDefault(); // display log in menu
			}
				return false;
			}; 
		
	},
	
	cookiecheck: function(){
		var accesstoken = App.Helpers.readCookie('access_token');
		if (accesstoken){
			App.settings.accesstoken = accesstoken;	
		}
	}
	
});	


