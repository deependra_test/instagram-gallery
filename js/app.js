window.App = {};
App.Models = {};
App.Collections = {};
App.Views = {};
App.Routers = {};
// for class instances
App.collections = {};
App.views = {};
App.settings = {clientid:'8caea5d76177434ab8cd8ab063f8a440'};

// helper functions
App.Helpers = {

	createCookie: function(name,value,days) {
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = "; expires="+date.toGMTString();
		}
		else var expires = "";
		document.cookie = name+"="+value+expires+"; path=/";
	},
	
	readCookie: function(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	},
	
	eraseCookie: function(name) {
		App.Helpers.createCookie(name,"",-1);
	},
	
	getaccessToken: function(id){
		var string = id;
		if (string.indexOf('access_token') != -1 ){ 
			var nvPairs = string.split("=");
			return nvPairs[1];
		} else {
			return null;
		}
	}

};
